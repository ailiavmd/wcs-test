import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Persona } from '../interfaces/Persona.interface';

@Injectable({
  providedIn: 'root'
})
export class ProfesoresService {

  constructor( private http:HttpClient ) { }

  obtenerProfesores():Observable<Persona[]> {
    return this.http.get<Persona[]>('http://hp-api.herokuapp.com/api/characters/staff')
  }
}
