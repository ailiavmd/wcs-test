import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Persona } from '../interfaces/Persona.interface';
import { Solicitud } from '../interfaces/Solicitud.interface';

@Injectable({
  providedIn: 'root'
})
export class EstudiantesService {

  constructor( private http: HttpClient ) { }

  obtenerEstudiantes():Observable<Persona[]> {
    return this.http.get<Persona[]>('http://hp-api.herokuapp.com/api/characters/students')
  }

  obtenerSolicitudes():Solicitud[] {
    const stored:any = localStorage.getItem('SOLICITUDES')
    const solicitudes:Solicitud[] = stored ? JSON.parse(stored) : []
    return solicitudes
  }

  agregarSolicitud(data:Solicitud):void {
    const stored:any = localStorage.getItem('SOLICITUDES')
    const solicitudes:Solicitud[] = stored ? JSON.parse(stored) : []
    solicitudes.push(data)
    localStorage.setItem('SOLICITUDES', JSON.stringify(solicitudes))
  }
}
