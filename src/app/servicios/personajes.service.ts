import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Persona } from '../interfaces/Persona.interface';

@Injectable({
  providedIn: 'root'
})
export class PersonajesService {

  constructor( private http: HttpClient ) { }

  obtenerPersonajes(casa:string):Observable<Persona[]> {
    return this.http.get<Persona[]>(`http://hp-api.herokuapp.com/api/characters/house/${casa}`)
  }
}
