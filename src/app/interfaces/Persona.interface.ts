interface Wand {
    wood:string
    core:string
    length:number
}

export interface Persona {
    name:string,
    alive?:boolean
    age?:number
    ancestry?:string
    dateOfBirth:string
    eyeColour?:string
    hogwartsStaff?:boolean
    hogwartsStudent?:boolean
    house:string
    image:string
    actor?:string
    patronus?:string
    species?:string
    wand?: Wand
    yearOfBirth?:number
}