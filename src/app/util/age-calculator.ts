import differenceInYears from 'date-fns/differenceInYears';

export function ageCalculator(dob:string):number {

    if (!dob || dob === '') return -1

    const dateStrings:string[] = dob.split('-')
    const dateNumbers:number[] = dateStrings.map((v:string) => parseInt(v))

    const birthDate = new Date(dateNumbers[2], (dateNumbers[1] - 1), dateNumbers[0])
    const today:Date = new Date()

    return differenceInYears(today, birthDate)
}