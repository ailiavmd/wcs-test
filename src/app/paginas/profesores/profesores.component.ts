import { Component, OnInit, ViewChild } from '@angular/core';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

import { ProfesoresService } from 'src/app/servicios/profesores.service';
import { Persona } from 'src/app/interfaces/Persona.interface';
import { ageCalculator } from 'src/app/util/age-calculator';

@Component({
  selector: 'app-profesores',
  templateUrl: './profesores.component.html',
  styles: []
})
export class ProfesoresComponent implements OnInit {

  displayedColumns: string[] = ['name', 'patronus', 'age', 'image'];
  dataSource: MatTableDataSource<Persona>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor( public service: ProfesoresService ) { }

  ngOnInit() {
    this.listar()
  }

  loading:boolean
  listar() {
    this.loading = true
    this.service.obtenerProfesores().subscribe(data => {
      console.log(data)
      data.forEach((p:Persona) => {if (p.dateOfBirth) p.age = ageCalculator(p.dateOfBirth)})
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      console.log(this.dataSource)
      this.loading = false
    }, error => {
      console.log(error)
      this.loading = false
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
