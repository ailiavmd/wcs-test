import { Component, OnInit, ViewChild } from '@angular/core';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

import { PersonajesService } from 'src/app/servicios/personajes.service';
import { Persona } from 'src/app/interfaces/Persona.interface';
import { ageCalculator } from 'src/app/util/age-calculator';

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styles: [
  ]
})
export class PersonajesComponent implements OnInit {

  displayedColumns: string[] = ['name', 'patronus', 'age', 'image'];
  dataSource: MatTableDataSource<Persona>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor( public service: PersonajesService ) { }

  casa:string = 'gryffindor'

  ngOnInit() {
    this.listar()
  }

  loading:boolean
  listar() {
    this.loading = true
    this.service.obtenerPersonajes(this.casa).subscribe(data => {
      console.log(data)
      data.forEach((p:Persona) => {if (p.dateOfBirth) p.age = ageCalculator(p.dateOfBirth)})
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.loading = false
    }, error => {
      console.log(error)
      this.loading = false
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
