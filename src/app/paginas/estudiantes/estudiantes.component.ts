import { Component, OnInit, ViewChild } from '@angular/core';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

import { EstudiantesService } from 'src/app/servicios/estudiantes.service';
import { Persona } from 'src/app/interfaces/Persona.interface';
import { ageCalculator } from 'src/app/util/age-calculator';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Solicitud } from 'src/app/interfaces/Solicitud.interface';
import { ModalComponent } from 'src/app/componentes/shared/modal/modal.component';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html',
  styles: ['.actions {margin-left:auto;}']
})
export class EstudiantesComponent implements OnInit {

  displayedColumns: string[] = ['name', 'patronus', 'age', 'image'];
  dataSource: MatTableDataSource<Persona>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('formModal') formModal:ModalComponent
  @ViewChild('tableModal') tableModal:ModalComponent

  constructor(
    public service: EstudiantesService,
    private fb: FormBuilder
  ) { }

  addForm:FormGroup
  ngOnInit() {
    this.listar()

    this.addForm = this.fb.group({
      name: ['', [Validators.required]],
      dateOfBirth: ['', [Validators.required]],
      house: ['', [Validators.required]],
    })
  }

  solicitudes:Solicitud[] = this.service.obtenerSolicitudes()

  loading:boolean
  listar() {
    this.loading = true
    this.service.obtenerEstudiantes().subscribe(data => {
      console.log(data)
      data.forEach((p:Persona) => {if (p.dateOfBirth) p.age = ageCalculator(p.dateOfBirth)})
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.loading = false
    }, error => {
      console.log(error)
      this.loading = false
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  agregarSolicitud() {
    const solicitud:Solicitud = {...this.addForm.value}
    solicitud.id = new Date().getTime()
    this.service.agregarSolicitud(solicitud)
    this.formModal.close()
    this.solicitudes = this.service.obtenerSolicitudes()
  }

}
