import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input('title') title:string
  @Input('subtitle') subtitle:string
  @Input('is-open') isOpen:boolean
  @Input('no-footer') noFooter:boolean
  @Input('close-on-confirm') closeOnConfirm:boolean = true
  @Input('disable-confirm') disableConfirm:boolean
  @Input('loading') loading:boolean
  @Input('large') large:boolean
  @Input('scroller') scroller:boolean
  @Output() 
  onclose: EventEmitter<any> = new EventEmitter()
  @Output()
  onconfirm: EventEmitter<any> = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
  }

  open() {
    this.isOpen = true
  }
  close() {
    this.isOpen = false
    this.onclose.emit()
  }
  confirm() {
    this.onconfirm.emit()
    if (this.closeOnConfirm) this.isOpen = false
  }

}
