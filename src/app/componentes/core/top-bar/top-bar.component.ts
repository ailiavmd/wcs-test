import { Component, OnInit } from '@angular/core';

type link = {
  title:string,
  path:string
}

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {

  constructor() { }

  activeLink:string = '/'

  links:link[] = [
    {
      title: 'Personajes',
      path: '/personajes'
    },
    {
      title: 'Estudiantes',
      path: '/estudiantes'
    },
    {
      title: 'Profesores',
      path: '/profesores'
    }
  ]

  ngOnInit(): void {
  }

}
