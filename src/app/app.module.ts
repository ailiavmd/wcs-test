import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './paginas/inicio/inicio.component';
import { CoreModule } from './componentes/core/core.module';
import { MatNativeDateModule } from '@angular/material/core';


const routes:Routes = [
  {
    path: '',
    component: InicioComponent
  },
  {
    path: 'estudiantes',
    loadChildren: () => import('./paginas/estudiantes/estudiantes.module').then(m => m.EstudiantesModule)
  },
  {
    path: 'personajes',
    loadChildren: () => import('./paginas/personajes/personajes.module').then(m => m.PersonajesModule)
  },
  {
    path: 'profesores',
    loadChildren: () => import('./paginas/profesores/profesores.module').then(m => m.ProfesoresModule)
  }
]

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    CoreModule,
    MatNativeDateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
